#!/bin/bash

# aws configure --profile cyberforce
# AWS Access Key ID [None]: *** [AWS_ACCESS_KEY_ID]
# AWS Secret Access Key [None]: *** [AWS_SECRET_ACCESS_KEY]
# Default region name [None]: eu-west-2
# Default output format [None]: json
eval $(aws ecr get-login --profile cyberforce --region eu-west-2 --no-include-email)

export dockerfile='./awscli/Dockerfile'
export IMAGE_NAME=916768316819.dkr.ecr.eu-west-2.amazonaws.com/images:awscli

docker build --file ${dockerfile} -t $IMAGE_NAME .
docker push $IMAGE_NAME